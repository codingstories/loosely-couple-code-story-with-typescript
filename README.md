# Refactor the tightly coupled code snippet

**To read**: [https://gitlab.com/coding-stories2/loosely-couple-code-story-with-typescript.git]

**Estimated reading time**: 10 mins

## Story Outline

In this coding story we will discuss how the tightly coupled code can be refactored into loosely couple code using a single responsibility principle.

## Story Organization

**Story Branch**: main

> `git checkout main`

Tags: #angular, #typescript, #jasmine, #solid
