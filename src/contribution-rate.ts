// Injecting State service as Angular selector input
<app-contribution-rates [service]="stateContributionRatesService"></app-contribution-rates>

// Injecting Federal service as Angular selector input
<app-contribution-rates [service]="federalContributionRatesService"></app-contribution-rates>

// Injecting Local service as Angular selector input
<app-contribution-rates [service]="localContributionRatesService"></app-contribution-rates>

@Input('service') contributionRatesService: ContributionRatesService;

// Getting method defination based on the service injection
this.columnNames = this.contributionRatesService.getColumnNames();