import { IContributionRatesService } from "./icontribution-rate.interface";

@Injectable({
  providedIn: CompanyTaxSetupServicesModule,
})
export class StateContributionRatesService
  implements IContributionRatesService
{
  columnNames: ContributionRatesColumnNames;

  constructor() {}

  getColumnNames(tab: string): ContributionRatesColumnNames {
    return {
      TcrUseSystemRate: {
        label: this.translateService.instant(
          "ukgpro.CompanyStateTaxSetup.UseSystemRate"
        ),
        disabled: true,
      },
      TcrContributionRate: {
        label: this.translateService.instant(
          "ukgpro.CompanyStateTaxSetup.ContributionRate"
        ),
        disabled: true,
      },
      TcrEffectiveDate: {
        label: this.translateService.instant(
          "ukgpro.CompanyStateTaxSetup.EffectiveDate"
        ),
        disabled: true,
      },
      TcrRateChangeReason: {
        label: this.translateService.instant(
          "ukgpro.CompanyStateTaxSetup.RateChangeReason"
        ),
        disabled: true,
      },
      TcrChangeReasonDesc: {
        label: this.translateService.instant(
          "ukgpro.CompanyStateTaxSetup.Description"
        ),
        disabled: true,
      },
      TcrBilledContributionRate: {
        label: this.translateService.instant(
          "ukgpro.CompanyStateTaxSetup.BillRate"
        ),
        disabled: true,
      },
      TcrBilledWageLimit: {
        label: this.translateService.instant(
          "ukgpro.CompanyStateTaxSetup.BillWageRate"
        ),
        disabled: true,
      },
      ActionItems: {
        label: " ",
        disabled: true,
        align: "right",
      },
    };
  }
}
