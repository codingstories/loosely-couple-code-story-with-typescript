@Injectable({
    providedIn: CompanyTaxSetupServicesModule
})

export interface IContributionRatesService {
    getColumnNames(): ContributionRatesColumnNames;
}