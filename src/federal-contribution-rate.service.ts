import { IContributionRatesService } from "./icontribution-rate.interface";

@Injectable({
  providedIn: CompanyTaxSetupServicesModule,
})
export class FederalContributionRatesService
  implements IContributionRatesService
{
  columnNames: ContributionRatesColumnNames;

  constructor() {}

  getColumnNames(tab: string): ContributionRatesColumnNames {
    return {
      TcrContributionRate: {
        label: this.translateService.instant("CompanyTaxNetTaxRate"),
        disabled: true,
      },
      TcrEffectiveDate: {
        label: this.translateService.instant(
          "ukgpro.CompanyStateTaxSetup.EffectiveDate"
        ),
        disabled: true,
      },
      TcrRateChangeReason: {
        label: this.translateService.instant(
          "ukgpro.CompanyStateTaxSetup.RateChangeReason"
        ),
        disabled: true,
      },
      TcrChangeReasonDesc: {
        label: this.translateService.instant(
          "ukgpro.CompanyStateTaxSetup.Description"
        ),
        disabled: true,
      },
      ActionItems: {
        label: " ",
        disabled: true,
        align: "right",
      },
    };
  }
}
