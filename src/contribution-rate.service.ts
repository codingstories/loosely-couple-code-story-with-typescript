@Injectable({
  providedIn: CompanyTaxSetupServicesModule,
})
export class ContributionRatesService {
  columnNames: ContributionRatesColumnNames;

  constructor() {}

  getColumnNames(tab: string): ContributionRatesColumnNames {
    if (tab === TaxProfileTab.Federal) {
      return {
        TcrContributionRate: {
          label: this.translateService.instant("CompanyTaxNetTaxRate"),
          disabled: true,
        },
        TcrEffectiveDate: {
          label: this.translateService.instant(
            "ukgpro.CompanyStateTaxSetup.EffectiveDate"
          ),
          disabled: true,
        },
        TcrRateChangeReason: {
          label: this.translateService.instant(
            "ukgpro.CompanyStateTaxSetup.RateChangeReason"
          ),
          disabled: true,
        },
        TcrChangeReasonDesc: {
          label: this.translateService.instant(
            "ukgpro.CompanyStateTaxSetup.Description"
          ),
          disabled: true,
        },
        ActionItems: {
          label: " ",
          disabled: true,
          align: "right",
        },
      };
    } else if (tab === TaxProfileTab.State) {
      return {
        TcrUseSystemRate: {
          label: this.translateService.instant(
            "ukgpro.CompanyStateTaxSetup.UseSystemRate"
          ),
          disabled: true,
        },
        TcrContributionRate: {
          label: this.translateService.instant(
            "ukgpro.CompanyStateTaxSetup.ContributionRate"
          ),
          disabled: true,
        },
        TcrEffectiveDate: {
          label: this.translateService.instant(
            "ukgpro.CompanyStateTaxSetup.EffectiveDate"
          ),
          disabled: true,
        },
        TcrRateChangeReason: {
          label: this.translateService.instant(
            "ukgpro.CompanyStateTaxSetup.RateChangeReason"
          ),
          disabled: true,
        },
        TcrChangeReasonDesc: {
          label: this.translateService.instant(
            "ukgpro.CompanyStateTaxSetup.Description"
          ),
          disabled: true,
        },
        TcrBilledContributionRate: {
          label: this.translateService.instant(
            "ukgpro.CompanyStateTaxSetup.BillRate"
          ),
          disabled: true,
        },
        TcrBilledWageLimit: {
          label: this.translateService.instant(
            "ukgpro.CompanyStateTaxSetup.BillWageRate"
          ),
          disabled: true,
        },
        ActionItems: {
          label: " ",
          disabled: true,
          align: "right",
        },
      };
    }
  }
}
