describe("FederalTaxDetailsComponent", () => {
  const coreModules = [
    HttpClientModule,
    CompanyTaxSetupServicesModule,
    RouterTestingModule,
    TranslateTestingModule.withTranslations(
      "en",
      MOCKTRANSLATIONS_DEFAULT_VALUE
    ),
  ];

  const coreProviders = [
    { provide: FederalTaxService, useValue: MOCKSERVICES.FEDERALTAX_SERVICE },
    {
      provide: CompanyTaxSetupStateService,
      useValue:
        companyTaxSetupStateMock.MOCKSERVICES.COMPANY_TAX_SETUP_SERVICES,
    },
    {
      provide: FederalTaxTableService,
      useValue: MOCKSERVICES.FEDERAL_TAXTABLE_SERVICE,
    },
  ];

  let component: FederalTaxDetailsComponent;
  const componentTest =
    new CompanyTaxSetupStateComponentTest<FederalTaxDetailsComponent>();

  const columnNames = {
    MttIfWageBaseIsOver: {
      label: "Over",
      disabled: true,
      visible: true,
    },
    MttIfWageBaseIsNotOver: {
      label: "NotOver",
      disabled: true,
      visible: true,
    },
    MttAmtOverWageBase: {
      label: "AmountOver",
      disabled: true,
      visible: true,
    },
    MttTaxPercentOverBase: {
      label: "TaxPercent",
      disabled: true,
      visible: true,
    },
    MttTaxAmt: {
      label: "TTTaxAmount",
      disabled: true,
      visible: false,
    },
    MttCapAmt: {
      label: "TTCapAmount",
      disabled: true,
      visible: false,
    },
    MttPercentOfFIT: {
      label: "PercentOfFIT",
      disabled: true,
      visible: false,
    },
    MttPercentOfSIT: {
      label: "PercentOfSIT",
      disabled: true,
      visible: false,
    },
    MttTaxCredit1: {
      label: "TTTaxCredit1",
      disabled: true,
      visible: true,
    },
    MttTaxCredit2: {
      label: "TTTaxCredit2",
      disabled: true,
      visible: true,
    },
    MttTaxCalcRule: {
      label: "TaxCalcRule",
      disabled: true,
      visible: false,
    },
    MttReducedTaxRate: {
      label: "TTTaxReducedTaxRate",
      disabled: true,
      visible: false,
    },
    MttFedSurTaxPct: {
      label: "TTTaxFlatPercentageRate",
      disabled: true,
      visible: true,
    },
    MttOthRateFactor: {
      label: "TTPercentageRate2",
      disabled: true,
      visible: true,
    },
    MttDateTimeChanged: {
      label: "DateTimeChanged",
      disabled: true,
      visible: true,
    },
    MttDateTimeCreated: {
      label: "DateTimeCreated",
      disabled: true,
      visible: true,
    },
  };

  beforeAll(async () => {
    // Setup TestBed
    await componentTest.createTestBed(
      [FederalTaxDetailsComponent],
      coreModules,
      coreProviders
    );
  });

  beforeEach(async () => {
    // Setup component
    component = componentTest.createComponent(FederalTaxDetailsComponent);
    const mockTaxTableService = jasmine.createSpyObj(
      "FederalContributionRatesService",
      ["getColumnNames"]
    );
    mockTaxTableService.getColumnNames.and.returnValue(columnNames);
    // wait for component to be created
    await componentTest.waitForComponentToBeStable();
  });

  afterAll(() => {
    // Reset the testing module
    componentTest.resetTestingModule();
  });

  describe("When View Page is loaded", () => {
    it("should create component", () => {
      expect(component).toBeDefined();
    });

    it("verify getColumnNames method response", () => {
      // Act
      component.ngOnInit();

      // Assert
      expect(component.columnNames).toEqual(columnNames);
    });
  });
});
